from deuxmasbot.stockings import Stocking, Gift, StockingCollection

import pytest

def test_gift_has_associated_sprite():
    g =  Gift.generate_gift()
    sprite = g.sprite
    open(sprite) # will except if invalid

def test_generates_gift():
    s = Stocking()
    assert s.gift

def test_stocking_collection_size():
    s1 = Stocking()
    s2 = Stocking()
    collection = StockingCollection()
    assert collection.stocking_count() == 0
    collection.add_stocking(s1)
    collection.add_stocking(s2)
    assert collection.stocking_count() == 2

def test_retrieve_stocking_by_index():
    collection = StockingCollection()
    s1 = Stocking()
    collection.add_stocking(s1)
    collection.add_stocking(s1)
    assert collection.get_stocking_at(1)

