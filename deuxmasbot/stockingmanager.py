from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from settings import CHAT_ID, PLAYERS
from image import stitcher
import stockings
from wand.image import Image
from io import BytesIO
import helper

USER_POINT_CALLBACK = None

class StockingManager:
  def __init__(self):
    self.stocking_collection = stockings.StockingCollection(count=4)
    self.stocking_choices = {}
    self.user_point_callback = None
    self.done = False
    self.orc = None
  
  def set_orc(self, orcinstance):
    self.orc = orcinstance

  def set_point_callback(self, c):
    self.user_point_callback = c

  def reset_stockings(self):
    self.stocking_collection.initialize_stockings()
    self.stocking_choices = {}
    self.done = False
    
  def select_stocking_for(self, *, player_id, stocking_index):
    stocking = self.stocking_collection.pick_stocking_at(int(stocking_index))
    self.stocking_choices[player_id] = stocking

  def stocking_menu(self, bot, update, args):
    indices = self.stocking_collection.get_unselected_indices()
    if indices.count == 0:
      self.done = True
      bot.send_message(CHAT_ID, "SORRY %s, THERE ARE NO MORE STOCKINGS." % update.effective_user.first_name)
      send_stocking_image(bot)
      return
    keyboard = [[]]
    for v in indices:
      keyboard[0].append(InlineKeyboardButton("STOCKING %d" % v, callback_data=v))
    keyboard = InlineKeyboardMarkup(keyboard)
    update.message.reply_text("CHOOSE A STOCKING: ", reply_markup=keyboard)

  def stocking_picked(self, bot, update):
    query = update.callback_query
    dont_select = False
    if update.effective_user.id in self.stocking_choices:
      bot.send_message(CHAT_ID, "ATTEMPTS TO SELECT MORE THAN ONE STOCKING WILL RESULT IN PUNISHMENT, %s" % update.effective_user.first_name)
      points = helper.random_points_in_range(-75, -25)
      self.user_point_callback(update.effective_user.id, points)
      dont_select = True

    if not update.effective_user.id == update.callback_query.message.reply_to_message.from_user.id:
      bot.send_message(CHAT_ID, "YOU MUSN'T SELECT STOCKINGS FOR OTHER USERS. NAUGHTY.")
      points = helper.random_points_in_range(-75, -25)
      self.user_point_callback(update.effective_user.id, points)
      dont_select = True

    if dont_select:
      return

    self.select_stocking_for(player_id=update.effective_user.id, stocking_index=query.data)
    bot.edit_message_text(text="SELECTED STOCKING: {}".format(query.data),
        chat_id=query.message.chat_id,
        message_id=query.message.message_id)
    if len(self.stocking_collection.get_unselected_indices()) == 0:
      self.send_stocking_image(bot)

  def send_stocking_image(self, bot):
    bot.send_message(CHAT_ID, "STOCKINGS HAVE BEEN SELECTED. WE SHALL FIND OUT WHO IS NAUGHTY AND WHO IS NICE.")
    self.generate_stocking_image_and_send(bot)

  def generate_stocking_image_and_send(self, bot):
    coords = [(10, 60), (136, 60), (252, 60), (368, 60), (10, 276), (130, 276), (248, 276), (364, 276)]
    user_img_coords =  [ (10, 276), (130, 276), (248, 276), (364, 276) ]
    img_stitcher = stitcher.ImageStitcher('images/stocking-frame.png', coords)
    for k, v in self.stocking_choices.items():
      # Draw the stocking
      stocking_sprite = Image(filename=v.gift.sprite)
      stocking_sprite.resize(width=96,height=96)
      img_stitcher.draw_sprite(stocking_sprite, rounded=False)
      photos = bot.get_user_profile_photos(k, limit=1).photos
      profile_image = photos[0]
      pi_file = bot.get_file(profile_image[0])
      downloaded = pi_file.download()
      person_image = Image(filename=downloaded)
      person_image.resize(width=96, height=96)
      index = self.stocking_collection.get_index_of(v)
      img_stitcher.draw_sprite(person_image, position=user_img_coords[index])

    bbytes = BytesIO()
    bbytes.name = "stockingimg.png"
    img_stitcher.image.save(bbytes)
    bbytes.seek(0)
    bot.send_photo(CHAT_ID, photo=bbytes)
    self.send_finished_string(bot)
  
  def send_finished_string(self, bot):
    finalized = "STOCKINGS HAVE BEEN GIVEN OUT. HERE IS THE POINT BREAKDOWN:\n"
    for user_key, choice in self.stocking_choices.items():
      choice_str = choice.gift.get_str()
      player_name = self.orc.player_state[user_key]['name']
      finalized += player_name + " gets " + choice_str + "\n"
      self.user_point_callback(user_key, choice.gift.points)
    bot.send_message(CHAT_ID, finalized)
