import os
import pickle
from os.path import isfile

from dotenv import load_dotenv

from pathlib import Path  # python3 only
env_path = Path('.') / '.env'
load_dotenv(dotenv_path='./.env', verbose=True)

def init_words(filename):
  words = []
  f = open(filename, 'r')
  for line in f.readlines():
    words.append(line.strip())
  return words

def save_players(players):
  print("SAVING PLAYERS", players)
  pickle.dump(players, open('players', 'wb'))
  print("SAVED")

WORD_POINTS = 3

DOG_DIRECTORY = "dogs"
USED_DOG_DIRECTORY = "dogs-used"

BOT_TOKEN = os.getenv("BOT_TOKEN")
print(BOT_TOKEN)
ADMIN_USER = int(os.getenv("admin_id"))
STORY_USER = int(os.getenv("story_id"))
#STORY_USER = int(os.getenv("admin_id"))

# TEST ONE
#CHAT_ID = -1001134044228

# TEST JOSH
#CHAT_ID = -1001307379030

# REAL ONE
CHAT_ID = -1001108782825

# Words and phrases
NAUGHTY_WORDS = init_words('naughty')
NICE_WORDS = init_words('nice')
MAGIC_WORDS = init_words('magic')

# Previous scores
if not isfile('players'):
  pickle.dump({}, open('players', 'wb'))

PLAYERS = pickle.load(open('players', 'rb'))
print("PLAYERS: %s" % PLAYERS)

USER_POINT_CALLBACK = None
