import random

gift_images = ['images/christmas-tree-gift.png', 'images/christmas-door-wreath.png', 'images/christmas-sugar-cane.png', 'images/christmas-sock.png']

gift_names = ['Coal','Holly','Candy Cane','Nothing']

gift_values = {}
gift_values['Coal'] = -55
gift_values['Holly'] = 30
gift_values['Candy Cane'] = 15
gift_values['Nothing'] = 0

class Gift:
    def generate_gift():
        return Gift(is_good=True, name="Awesome Gift", sprite='images/christmas-sock.png', points=50)

    def __init__(self, is_good, name, sprite, points=0):
        self.is_good = is_good
        self.name = name
        self.points = points
        self.sprite = sprite

    def get_str(self):
        s = self.name + " worth "
        if self.is_good:
            s = s + "+" + str(self.points)
        else:
            s = s + "-" + str(self.points)
        return s + " points." 

class Stocking:
    def __init__(self, gift=Gift.generate_gift()):
        self.gift = gift
        self.picked = False

    def pick(self):
      self.picked = True

class StockingCollection:
    stockings = []

    def __init__(self, count=4):
        self.count = count
        self.initialize_stockings()

    def initialize_stockings(self):
        self.stockings = []
        for i in range(0, self.count):
          image = random.choice(gift_images)
          name = random.choice(gift_names)
          points = gift_values[name]
          gift = Gift(is_good=(gift_values[name] > 0), name=name, sprite=image, points=points)
          self.stockings.append(Stocking(gift))

    def add_stocking(self, stocking): 
        self.stockings.append(stocking)

    def stocking_count(self):
        return len(self.stockings)

    def get_index_of(self, stocking):
      for index, s in enumerate(self.stockings):
        if s == stocking:
          return index
      return -1

    def get_stocking_at(self, index):
        return self.stockings[index]

    def pick_stocking_at(self, index):
      self.stockings[index].pick()
      return self.stockings[index]

    def get_unselected_indices(self):
      indices = []
      for index, stocking in enumerate(self.stockings):
        if not stocking.picked:
          indices.append(index)
      return indices
    
    def get_finished_string(self):
        finished = ""
        for stocking in self.stockings:
            finished += stocking.gift.name + " worth "
            if stocking.gift.is_good:
                finished += " +" 
            else:
                finished += " -" 
            finished += str(stocking.gift.points) + "points\n"
        return finished

