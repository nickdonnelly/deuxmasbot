import telegram
from settings import CHAT_ID

class PollManager:
  def __init__(self):
    self.is_poll_active = False
  
  def start_poll(self, question, bot, userid):
    self.question = question
    self.options = []
    self.voters = []
    self.bot = bot
    self.poll_user = userid
    self.is_poll_active = True
    self.isvoting = False

  def poll_active(self):
    return self.is_poll_active

  def finish_poll(self):
    self.isvoting = True
  
  def stop_poll(self):
    self.options = []
    self.voters = []
  
  def check_is_done(self, vote_count):
    if len(self.voters) >= vote_count:
      self.bot.send_message(CHAT_ID, self.poll_string())

  def add_option(self, option):
    if self.isvoting or not self.is_poll_active:
      return
    opt = {}
    opt['string'] = option
    opt['votes'] = 0
    self.options.append(opt)
  
  def vote_for_option(self, index, user):
    if 0 <= index and index < len(self.options) and user not in self.voters:
      self.options[index]['votes']  += 1
      self.voters.append(user)
      return True
    return False

  def poll_string(self):
    printable = "HERE📃IS📃YOUR📃POLL:\n"
    printable += self.question + "\n"
    count = 1
    for opt in self.options:
      printable += str(count) + ") " + opt['string'] + ": " + str(opt['votes']) + " votes\n"
      count += 1 
    return printable