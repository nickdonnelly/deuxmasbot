from settings import NAUGHTY_WORDS, NICE_WORDS, MAGIC_WORDS, WORD_POINTS
USER_ADDED_CALLBACK = None
USER_POINT_CALLBACK = None
USER_STOCKING_CALLBACK = None
USER_STOCKING_PICK_CALLBACK = None

def scorer(bot, update, job_queue):
  text = update.message.text
  isMean = False
  for word in NAUGHTY_WORDS:
    if word in text:
      isMean = True
  for word in NICE_WORDS:
    if word.strip() in text and isMean:
      USER_POINT_CALLBACK(update.effective_user.id, -1 * WORD_POINTS, silent=True) # take points away
    elif word.strip() in text:
      USER_POINT_CALLBACK(update.effective_user.id, WORD_POINTS, silent=True) # give points

def background(bot, update, job_queue):
  if update.effective_user: #is sometimes none for channel_post
    if USER_ADDED_CALLBACK:
      USER_ADDED_CALLBACK(update.effective_user)
  scorer(bot, update, job_queue)

def stocking(bot, update, args):
  USER_STOCKING_CALLBACK(bot, update, args)

def stocking_picked(bot, update):
  USER_STOCKING_PICK_CALLBACK(bot, update)
