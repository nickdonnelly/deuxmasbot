# This is is the main bot file.

# Standard
import logging

# External
import telegram
from telegram import InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, InlineQueryHandler

# Internal
from settings import BOT_TOKEN
from stockings import StockingCollection
from orchestrator import Orchestrator
from command import admin, story, general
from image import stitcher
import handler, dog
from wand.image import Image
from wand.color import Color
from io import BytesIO

# Get a bot and an updater
bot = telegram.Bot(token=BOT_TOKEN)

orchestrator = Orchestrator()
orchestrator.set_bot(bot)

# Initialize the command handlers
orchestrator.add_handler(CommandHandler("scores", admin.scores, pass_args=True))
orchestrator.add_handler(CommandHandler("stockings", admin.stockings, pass_args=True))
orchestrator.add_handler(CommandHandler("stocking", handler.stocking, pass_args=True))
orchestrator.add_handler(CommandHandler("forcestate", admin.force_state, pass_args=True))
orchestrator.add_handler(CommandHandler("botsay", admin.botsay, pass_args=True))
orchestrator.add_handler(CommandHandler("points", admin.modify_points, pass_args=True))
orchestrator.add_handler(CommandHandler("dog", dog.dog_cmd, pass_args=True))

# Story command handlers
orchestrator.add_handler(CommandHandler("story", story.story, pass_args=True))
orchestrator.add_handler(CommandHandler("vote", story.vote_on_story, pass_args=True))

# General commands
orchestrator.add_handler(CommandHandler("roll", general.roll_command, pass_args=True))
orchestrator.add_handler(CommandHandler("poll", general.poll_command, pass_args=True))
orchestrator.add_handler(CommandHandler("pollvote", general.poll_vote, pass_args=True))

# Set up the logger
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# Start the bot
orchestrator.start_bot()

'''
def echo(bot, update):
    profile_photos = bot.get_user_profile_photos(update.message.from_user.id, limit=1).photos
    prof_img = profile_photos[0]
    f = bot.get_file(prof_img[0])
    downloaded = f.download()

    collection = StockingCollection(count=4)
    collection.initialize_stockings()

    coords = [(10, 60), (136, 60), (252, 60), (368, 60), (10, 276), (130, 276), (248, 276), (364, 276)]
    s = stitcher.ImageStitcher('images/stocking-frame.png', coords)

    for i in range(0, 4):
        stocking = collection.get_stocking_at(i)
        sprite = Image(filename=stocking.gift.sprite)
        sprite.resize(width=96, height=96)
        s.draw_sprite(sprite, rounded=False)

    person_image = Image(filename=downloaded)
    person_image.resize(width=96,height=96)

    s.draw_sprite(person_image)
    s.draw_sprite(person_image)
    s.draw_sprite(person_image)
    s.draw_sprite(person_image)

    bio = BytesIO()
    bio.name = "imagetest.png"
    s.image.save(bio)
    bio.seek(0)
    bot.send_photo(update.message.chat_id, photo=bio)

    echo_handler = MessageHandler(Filters.group, echo)
    dispatcher.add_handler(echo_handler)

score_command_handler = CommandHandler("scores", admin.scores, pass_args=True)
stocking_command_handler = CommandHandler("stockings", admin.stockings, pass_args=True)
state_command_handler = CommandHandler("force_state", admin.force_state, pass_args=True)

dispatcher.add_handler(score_command_handler)
dispatcher.add_handler(stocking_command_handler)
dispatcher.add_handler(state_command_handler)
    '''

