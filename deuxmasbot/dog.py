import os
import telegram
from os.path import isfile, join
import random
from settings import DOG_DIRECTORY, USED_DOG_DIRECTORY, CHAT_ID

# Makes the directories for dogs if they dont exist
def check_dirs():
  if not os.path.isdir(DOG_DIRECTORY):
    os.mkdir(DOG_DIRECTORY)
  if not os.path.isdir(USED_DOG_DIRECTORY):
    os.mkdir(USED_DOG_DIRECTORY)

def get_dog():
  check_dirs()
  files = [f for f in os.listdir(DOG_DIRECTORY) if not f.startswith(".") and isfile(join(DOG_DIRECTORY, f))]
  dog = random.choice(files)
  os.rename(join(DOG_DIRECTORY, dog), join(USED_DOG_DIRECTORY, dog))
  return open(join(USED_DOG_DIRECTORY, dog), 'rb')
  
def dog_cmd(bot, update, args):
  dog_bytes = get_dog()
  dog_bytes.seek(0)
  bot.send_message(CHAT_ID, "'TIS🎄THE🎄SEASON🎄FOR🎄CUTE🎄DOGS")
  bot.send_photo(CHAT_ID, photo=dog_bytes)