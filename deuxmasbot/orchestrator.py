import random
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Handler
from telegram.ext.filters import Filters
from settings import BOT_TOKEN, PLAYERS, STORY_USER, save_players, CHAT_ID
import settings
import handler, stockingmanager, storymanager
from pollmanager import PollManager

class Orchestrator:
  STATE_RESPONSE = 0
  STATE_STOCKING = 1
  STATE_VOTE_STORY = 2
  STATE_RAFFLE = 2
  instance = None
  class __Orchestrator:
    def __init__(self):
      self.poll_active = False
      self.group_members = Orchestrator.load_member_ids()
      self.player_state = PLAYERS
      self.current_state = Orchestrator.STATE_RESPONSE
      self.updater = Updater(token=BOT_TOKEN)
      self.poll_manager = PollManager()
      self.dispatcher = self.updater.dispatcher
      self.stocking_manager = stockingmanager.StockingManager()
      storymanager.USER_POINT_CALLBACK = Orchestrator.change_points_by
      handler.USER_ADDED_CALLBACK = Orchestrator.add_member
      handler.USER_POINT_CALLBACK = Orchestrator.change_points_by
      handler.USER_STOCKING_CALLBACK = self.stocking_manager.stocking_menu
      handler.USER_STOCKING_PICK_CALLBACK = self.stocking_manager.stocking_picked
      self.story_manager = storymanager.StoryManager()
      self.stocking_manager.set_point_callback(Orchestrator.change_points_by)
      self.stocking_manager.set_orc(self)
      self.background_handler = MessageHandler(Filters.text, handler.background, pass_job_queue=True)
      self.dispatcher.add_handler(self.background_handler)

    def set_bot(self, bot):
      self.bot = bot

    def set_state(self, state, handler=None):
      self.current_state = state
      for h in self.dispatcher.handlers:
        if not isinstance(h, CommandHandler):
          self.dispatcher.remove_handler(h)
          self.add_handler(self.background_handler)

      if not handler is None:
        self.dispatcher.add_handler(handler)

    def add_handler(self, handler):
      self.dispatcher.add_handler(handler)

    def set_message_handler(self, new_handler):
      for handler in self.dispatcher.handlers:
        if isinstance(handler, MessageHandler):
          self.dispatcher.remove_handler(handler)
      self.dispatcher.add_handler(new_handler)
      self.add_defaults()

    def start_bot(self):
      self.updater.start_polling()

  def add_member(member):
    member_id = member.id
    name = member.first_name
    if member_id == STORY_USER:
      return
    if not str(member_id) in Orchestrator.instance.group_members and not name == 'Joshua':
      Orchestrator.instance.group_members.append(str(member_id).strip())
      Orchestrator.instance.player_state[member_id] = { 'points': 0, 'wins': 0, 'losses': 0, 'name': name }
      Orchestrator.save_members(Orchestrator.instance.group_members)

  def change_points_by(player, points, silent=False):
    print('player:',player)
    Orchestrator.instance.player_state[player]['points'] += points
    save_players(Orchestrator.instance.player_state)
    if silent:
      return
    msg = "*%s*" % Orchestrator.instance.player_state[player]['name']
    if points == 0:
      return
    elif points > 0:
      msg = msg + " gains "
    else:
      msg = msg + " loses "
    msg = msg + str(points) + " points."
    Orchestrator.instance.bot.send_message(CHAT_ID, msg, parse_mode=telegram.ParseMode.MARKDOWN)

  def get_story_manager(self):
    return self.story_manager
  
  def load_member_ids():
    f = open('members', 'r')
    members = []
    for line in f:
      if line.strip() == '':
        continue
      members.append(line.strip())

    f.close()
    return members
  
  def save_members(members):
    f = open('members', 'w+')
    for m in members:
      f.write(str(m) + '\n')
    x = f.close()
    save_players(Orchestrator.instance.player_state)

  def __init__(self):
    if not Orchestrator.instance:
      Orchestrator.instance = Orchestrator.__Orchestrator()

  def __getattr__(self, name):
    if name == 'instance':
      return self.instance
    return getattr(self.instance, name)

