import time
import helper
from settings import CHAT_ID

USER_POINT_CALLBACK = None

class StoryManager:
  def __init__(self):
    self.players_whove_voted = []
    self.story_points = []
    self.story_primary = ""

  def clear_story(self):
    self.story_primary = ""
    self.story_points = []
    self.players_whove_voted = []

  def set_story_primary(self, primary):
    self.story_primary = primary

  def add_story_point(self, story):
    point = { 'votes': 0, 'string': story }
    self.story_points.append(point)

  def add_vote_to_index(self, ind, user, bot):
    if ind < 0 or ind >= len(self.story_points):
      bot.send_message(CHAT_ID, "YOU MUST PROVIDE A NUMBER BETWEEN 0 AND %d" % (len(self.story_points - 1)))
      return
    
    if user in self.players_whove_voted:
      bot.send_message(CHAT_ID, "YOU MAY ONLY VOTE ONCE.")
      points = helper.random_points_in_range(-75, -25)
      USER_POINT_CALLBACK(user.id, points)
      return

    self.players_whove_voted.append(user)
    self.story_points[ind]['votes'] += 1
    bot.send_message(CHAT_ID, "REST ASSURED YOUR VOTE HAS BEEN COUNTED")

  def get_primary(self):
    return self.story_primary

  def get_story_string(self):
    result = ""
    for point in self.story_points:
      result += "%s: %d points" % (point['string'], point['votes'])
      result += "\n"
    return result

  def finalize_story(self, bot):
    bot.send_message(CHAT_ID, "GATHER ROUND, IT IS TIME FOR ANOTHER PART OF THE STORY")
    time.sleep(2)
    bot.send_message(CHAT_ID, self.get_primary())
    time.sleep(10)
    bot.send_message(CHAT_ID, "VOTE ON THE FOLLOWING USING /vote:")
    bot.send_message(CHAT_ID, self.get_story_string())

  def complete_story(self, bot):
    bot.send_message(CHAT_ID, "VOTING HAS BEEN COMPLETED. AND THE WINNER IS....")
    max_votes = 0
    max_vote_index = 0
    for index, option in enumerate(self.story_points):
      if option['votes'] > max_votes:
        max_votes = option['votes']
        max_vote_index = index
    bot.send_message(CHAT_ID, "OPTION " + str(max_vote_index + 1) + "!")
    bot.send_message(CHAT_ID, "TUNE IN TOMORROW TO SEE WHAT COMES NEXT.")