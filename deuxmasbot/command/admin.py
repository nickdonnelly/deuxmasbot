import time
from functools import wraps
import telegram
from handler import stocking_picked
from telegram.ext import MessageHandler, CallbackQueryHandler, Filters
from settings import ADMIN_USER, CHAT_ID
from orchestrator import Orchestrator

orc = Orchestrator()

def restricted(func):
  @wraps(func)
  def wrapped(bot, update, *args, **kwargs):
    user_id = update.effective_user.id
    if user_id != ADMIN_USER:
      print("Unauthorized use of admin command by non-admin: %d" % user_id)
      return
    return func(bot, update, *args, **kwargs)
  return wrapped

@restricted
def scores(bot, update, args):
  score_str = "HO🎅🏻HO🎅🏻HO"
  score_str += "\nIT IS TIME FOR THE SCORES"
  score_str += "\nWHO HAS BEEN NAUGHTY⚫️AND WHO HAS BEEN NICE🎁?"
  
  bot.send_message(CHAT_ID, score_str)
  time.sleep(1)
  bot.send_message(CHAT_ID, "📜TABULATING...📜")

  max_player = 0
  max_score = 0
  score_str = "HERE🎄ARE🎄THE🎄RESULTS"
  for key, player in orc.player_state.items():
    score_str += "\n*%s* has _%d_ points" % (player['name'], player['points'])
    if player['points'] > max_score:
      max_score = player['points']
      max_player = key

  score_str += "\n*%s*🎆is🎆the🎆winner🎆this🎆week🎆" % orc.player_state[max_player]['name']
  orc.player_state[max_player]['wins'] = orc.player_state[max_player]['wins'] + 1
  time.sleep(5)
  bot.send_message(CHAT_ID, score_str, parse_mode=telegram.ParseMode.MARKDOWN)

@restricted
def stockings(bot, update, args):
  chat_id = update.message.chat_id
  orc.stocking_manager.reset_stockings()
  orc.set_state(Orchestrator.STATE_STOCKING, CallbackQueryHandler(stocking_picked))
  bot.send_message(CHAT_ID, "Starting stockings...Pick a stocking with /stocking")

def force_state(bot, update, args):
  state = args[0].lower()
  chat_id = update.message.chat_id
  msg = "Could not update state to %s" % state
  if args[0] == 'response':
    orc.set_state(Orchestrator.STATE_RESPONSE, STOCKING_HANDLER)
    msg = "BE CAREFUL WHAT YOU SAY, I AM MAKING A LIST AND CHECKING IT TWICE. STATE IS REPONSE MODE."
  elif args[0] == 'stocking':
    stockingHandler = MessageHandler(Filters.group, stockings)
    print("is stockingHandler instance? %s" % isinstance(stockingHandler, Handler))
    orc.set_state(Orchestrator.STATE_STOCKING, stockingHandler)
    msg = "BEEP BOOP. STATE IS STOCKING"
  elif args[0] == 'vote':
    orc.set_state(Orchestrator.STATE_VOTE_STORY, STOCKING_HANDLER)
    msg = "VOTING MODE ENABLED. GET YOUR VOTES IN FOR THE NEXT PART OF THE CHRISTMAS STORY."
  elif args[0] == 'raffle':
    orc.set_state(Orchestrator.STATE_RAFFLE, STOCKING_HANDLER)
    msg = "STARTING RAFFLE MODE. LET'S SEE WHO HAS CHRISTMAS LUCK AND WHO DOES NOT."

  bot.send_message(chat_id, msg)

@restricted
def botsay(bot, update, args):
  tosay = ' '.join(args)
  bot.send_message(CHAT_ID, tosay)

@restricted
def modify_points(bot, update, args):
  name = args[0].lower()
  points = int(args[1])
  for k, player in orc.player_state.items():
    if player['name'].lower() == name:
      Orchestrator.change_points_by(k, points)
