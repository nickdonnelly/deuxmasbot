from functools import wraps
from settings import ADMIN_USER, STORY_USER, CHAT_ID
from orchestrator import Orchestrator

orc = Orchestrator()

def restricted(func):
  @wraps(func)
  def wrapped(bot, update, *args, **kwargs):
    user_id = update.effective_user.id
    if user_id != ADMIN_USER and user_id != STORY_USER:
      print("Unauthorized use of story command by non-admin: %d" % user_id)
      return
    return func(bot, update, *args, **kwargs)
  return wrapped

@restricted
def next_story_point(bot, update, args):

  print(update.message.text)

@restricted
def story(bot, update, args):
  if len(args) == 0:
    bot.send_message(update.message.chat_id, "PLEASE PROVIDE A COMMAND: reset, main, option, finalize")
    return

  if args[0] == "reset":
    orc.get_story_manager().clear_story()
    bot.send_message(update.message.chat_id, "STORY HAS BEEN CLEARED.")
  elif args[0] == "main":
    if len(args) < 2:
      bot.send_message(update.message.chat_id, "PLEASE PROVIDE A STORY SECTION.")
      return
    else:
      orc.get_story_manager().set_story_primary(' '.join(args[1:]))
      bot.send_message(update.message.chat_id, "SET STORY MAIN AS FOLLOWS: \n%s" % ' '.join(args[1:]))
  elif args[0] == "option":
    if len(args) < 2:
      bot.send_message(update.message.chat_id, "PLEASE PROVIDE A STORY OPTION")
      return
    orc.get_story_manager().add_story_point(' '.join(args[1:]))
    bot.send_message(update.message.chat_id, orc.get_story_manager().get_story_string())
  elif args[0] == "finalize":
    orc.get_story_manager().finalize_story(bot)
    bot.send_message(update.message.chat_id, "FINALIZED STORY AND SENT FOR VOTING")
  elif args[0] == "endvoting":
    orc.get_story_manager().complete_story(bot)
    bot.send_message(update.message.chat_id, "VOTING COMPLETE")
  else:
    bot.send_message(update.message.chat_id, "UNKNOWN COMMAND: %s" % args[0])

def vote_on_story(bot, update, args):
  if len(args) != 1:
    bot.send_message(CHAT_ID, "PLEASE PROVIDE ONE NUMBER ARGUMENT")
    return

  orc.get_story_manager().add_vote_to_index(int(args[0]) - 1, update.effective_user, bot)
