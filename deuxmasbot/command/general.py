import telegram
import random
import helper
from orchestrator import Orchestrator
from settings import CHAT_ID

orc = Orchestrator()

def roll_command(bot, update, args):
  low = 0 
  high = 100
  if len(args) == 2:
    try:
      low = int(args[0])
      high = int(args[1])
    except:
      low = 0
      high = 100

  if low == high or low > high:
    bot.send_message(CHAT_ID, "NAUGHTY➕INPUT➗LOWER< UPPER.\nUSAGE: /roll [LOWER] [UPPER]")
    points = helper.random_points_in_range(-10, -5)
    Orchestrator.change_points_by(update.effective_user.id, points)
    return
  value = random.randrange(low, high)
  msg = ""
  perc = (value - low) / (high - low)

  if perc >= 0.85:
    msg = "HO🎲HO🎲HOLY🎲COW!🎲YOU🎲ROLLED🎲A🎲*%d*🎲FROM🎲THE🎲RANGE🎲*%d-%d*" % (value, low, high)
  elif perc <= 0.15:
    msg = "BAH🎲HUMBUG!🎲YOU🎲ROLLED🎲A🎲*%d*🎲FROM🎲THE🎲RANGE🎲*%d-%d*" % (value, low, high)
  else:
    msg = "YOU🎲ROLLED🎲A🎲*%d*🎲FROM🎲THE🎲RANGE🎲*%d-%d*" % (value, low, high)

  bot.send_message(CHAT_ID, msg, parse_mode=telegram.ParseMode.MARKDOWN)

# Starts a poll
def poll_command(bot, update, args):
  if orc.poll_manager.poll_active() and not args[0] == 'option' and not args[0] == 'start':
    bot.send_message(CHAT_ID, "THERE📃IS📃ALREADY📃A📃POLL📃ACTIVE")
    points = helper.random_points_in_range(-15, 0)
    Orchestrator.change_points_by(update.effective_user.id, points)
    return

  if args[0] == 'option':
    if not update.effective_user.id == orc.poll_manager.poll_user:
      points = helper.random_points_in_range(-200, 0)
      bot.send_message(CHAT_ID, "DO🚫NOT🚫MAKE🚫OPTIONS🚫FOR🚫OTHER🚫USER'S🚫POLLS!")
      Orchestrator.change_points_by(update.effective_user.id, points)
      return
    option = ' '.join(args[1:])
    orc.poll_manager.add_option(option)
    bot.send_message(CHAT_ID, "➕OPTION➕ADDED➕")
    return

  if args[0] == 'start' and update.effective_user.id == orc.poll_manager.poll_user:
    orc.poll_manager.finish_poll()
    bot.send_message(CHAT_ID, "POLL STARTDED. VOTE✅USING✅/pollvote")
    return

  question = ' '.join(args)
  orc.poll_manager.start_poll(question, bot, update.effective_user.id)
  bot.send_message(CHAT_ID, "STARTED POLL WITH QUESTION:\n" + question)

def poll_vote(bot, update, args):
  if not orc.poll_manager.poll_active() or not orc.poll_manager.isvoting:
    bot.send_message(CHAT_ID, "NO POLL AVAILABLE FOR VOTING. NAUGHTY.")
    points = helper.random_points_in_range(-15, 0)
    Orchestrator.change_points_by(update.effective_user.id, points)
    return
  points = helper.random_points_in_range(-15, 0)
  try:
    option = int(args[0])
    if not orc.poll_manager.vote_for_option(option, update.effective_user.id):
      bot.send_message(CHAT_ID, "DUPLICATE VOTES ARE NAUGHTY.")
      Orchestrator.change_points_by(update.effective_user.id, points)
    orc.poll_manager.check_is_done(len(orc.player_state))
  except:
    bot.send_message(CHAT_ID, "WHOLE NUMBERS IN THE CORRECT RANGE ONLY PLEASE. NAUGHTY.")
    Orchestrator.change_points_by(update.effective_user.id, points)